ensure_common_pkgs_are_installed:
  pkg.installed:
    - pkgs:
      - git 
      - curl 
      - wget
      - python3-pip
  pip.installed:
    - names:
      - boto3

ensure_info_file_is_created:
  file.managed:
    - name: /tmp/system_info.conf
    - source: salt://common/templates/file.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 755