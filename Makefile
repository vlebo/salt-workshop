# make VERBOSE nonempty to see raw commands (or provide on command line)
ifndef VERBOSE
VERBOSE:=
MAKEFLAGS += --no-print-directory
endif

# use SHOW to inform user of commands
SHOW:=@echo

# use HIDE to run commands invisibly, unless VERBOSE defined
HIDE:=$(if $(VERBOSE),,@)

##
##|------------------------------------------------------------------------|
##			Help
##|------------------------------------------------------------------------|
help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

DOCKER:= docker
DOCKER_COMPOSE:= docker-compose
SHELL:=/bin/bash
DOCKER_COMPOSE_FILE:= development/docker-compose.yml
INIT_TARGETS:= copy-docker-ignore docker-build

init: $(INIT_TARGETS)
	$(HIDE) make init-done

##
##|------------------------------------------------------------------------|
##			Docker
##|------------------------------------------------------------------------|
docker-list: ## List available services
	$(SHOW) "--  Services  --"
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) config --services

docker-build: ## Build all or c=<name> development containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) build --parallel $(c)

docker-rebuild: ## Rebuild w/o cache all or c=<name> development containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) build --no-cache $(c)

docker-up: ## Start all or c=<name> development containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up $(c)

docker-start: ## Start all or c=<name> development containers in background
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d $(c)

docker-stop: ## Stop all or c=<name> development containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)

docker-restart: ## Restart all or c=<name> development containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d $(c)

docker-logs: ## Show logs for all or c=<name> development containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) logs --tail=100 -f $(c)

docker-clean:  ## Clean all data
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down -v
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE_CORE) down -v

docker-status: ## Show status of development containers
	$(SHOW) "--  Services  --"
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) ps

docker-prune: ## Delete dangling images
	$(SHOW) Deleting dangling docker images
	$(HIDE) docker system prune

##|------------------------------------------------------------------------|
	
init-done:
	$(SHOW) " "
	$(SHOW) " --------------------------"
	$(SHOW) " Initialization done!"
	$(SHOW) " --------------------------"
	$(SHOW) " "
	$(SHOW) -e "Run \e[1mmake docker-up \e[0mto run all services!"
	$(SHOW) " "
	$(SHOW) -e " \e[5mHappy coding! :) \e[0m"

copy-docker-ignore: 
	$(eval CWD_ON_HOST := $(shell scripts/cwd_on_host.sh ))
	${HIDE} cp .dockerignore ${CWD_ON_HOST}/../

