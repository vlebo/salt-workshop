# DORS/CLUC 2022 SaltStack Workshop

## About
This directory contains Docker configuration used for building images and
running containers/services needed for the workshop.

[Docker](https://docs.docker.com/) images are created with `Dockerfile`s.

[docker-compose](https://docs.docker.com/compose/overview/) is used to describe
the desired state of all services, thus implementing an Infrastructure as
Code (IaC) approach.

[salt-api](https://docs.saltproject.io/en/latest/ref/netapi/all/salt.netapi.rest_cherrypy.html)
## Quick start
**Prerequisites:**
  - Install [Docker](https://docs.docker.com/install/) tested on version `20.10.8`
  - Install [docker-compose](https://docs.docker.com/compose/install/) tested on version `1.27.4`

#### Setup project structure
Create a direcotry where workshop should be. i.e.
```bash
mkdir -p ~/projects/dors2022/
```

Place your self in previously created directory and clone salt-workshop repo:

```bash
cd ~/projects/dors2022/ && git clone git@github.com:vlebo/salt-workshop.git
```

```
#### Adding hostnames
Add hostnames to `/etc/hosts`:
```bash
127.0.0.1  salt.tool.local
```

#### Available commands
To list all available commands run:
```bash
make help

|------------------------------------------------------------------------|
            Help
|------------------------------------------------------------------------|
help:  Show this help.

|------------------------------------------------------------------------|
            Docker
|------------------------------------------------------------------------|
docker-list:  List available services
docker-build:  Build all or c=<name> development containers in foreground
docker-rebuild:  Rebuild w/o cache all or c=<name> development containers in foreground
docker-up:  Start all or c=<name> development containers in foreground
docker-start:  Start all or c=<name> development containers in background
docker-stop:  Stop all or c=<name> development containers
docker-restart:  Restart all or c=<name> development containers
docker-logs:  Show logs for all or c=<name> development containers
docker-clean:   Clean all data
docker-status:  Show status of development containers
docker-prune:  Delete dangling images
|------------------------------------------------------------------------|

```
#### Initialize project
This will initialize the project including pulling and building all images. 
```bash
$ make init
```

#### Run all containers
To run all containers at once run the command:
```bash
$ make docker-up
```

To run all containers at once in detached mode run the command:
```bash
$ make docker-start
```

#### List available containers
To list all containers run:
```bash
$ make docker-list
```
--  Services  --
minion1
minion2
db
salt
nginx
```

#### Build application images
**NOTE:** This might take some time depending on your computer hardware. 

To build all images run command:
```bash
$ make docker-build
```

#### Build single container
To build single container run command:
```bash
$ make docker-build c=CONTAINER_NAME
```

#### Rebuild single container with cache busting
To rebuild single container w/o cache:
```bash
$ make docker-rebuild c=CONTAINER_NAME
```

## Connect into containers
As application user
```bash
$ docker exec -ti CONTAINER_NAME bash
```
As root user
```bash
$ docker exec -ti -u 0 CONTAINER_NAME bash
```

## Logs
All logs are in `logs` direcotry of this project together with `stdout`
To tail logs of a single container run command:
```bash
make docker-logs c=CONTAINER_NAME
```

## Salt-API
Example usage from localhost:
Login:
```
╰─> curl -sSk https://salt.tool.local/login \
    -H 'Accept: application/x-yaml' \
    -d username=salt-api \
    -d password=salt-api \
    -d eauth=pam
return:
- eauth: pam
  expire: 1636717036.6477962
  perms:
  - .*
  - '@runner'
  - '@wheel'
  - '@jobs'
  start: 1636673836.647796
  token: f4f1a57d9c5125bb00f4446a1426a89cc10f52a6
  user: salt-api
```

Request:
```
╰─> curl -sSk https://salt.tool.local \
    -H 'Accept: application/x-yaml' \
    -H 'X-Auth-Token: f4f1a57d9c5125bb00f4446a1426a89cc10f52a6'\
    -d client=local \
    -d tgt='*' \
    -d fun=test.ping
return:
- master: true
  minion1: true
  minion2: true
```